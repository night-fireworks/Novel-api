package com.novel.resource.qiniu.config;

import com.qiniu.storage.Region;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * QINIU 配置
 *
 * @author novel
 * @date 2020/3/31
 */
@ConfigurationProperties(prefix = QiniuConfig.QINIU_PREFIX)
public class QiniuConfig {
    public static final String QINIU_PREFIX = "qiniu";
    /**
     * 容器id
     */
    private String qiniuAccessKey;
    /**
     * 容器秘钥
     */
    private String qiniuSecretKey;
    /**
     * 容器的名称
     */
    private String bucketName;
    /**
     * 容器子域名
     */
    private String domainOfBucket;

    /**
     * 区域名称： 华东   华北   华南   北美   东南亚
     */
    private QiniuRegion region;

    public String getQiniuAccessKey() {
        return qiniuAccessKey;
    }

    public void setQiniuAccessKey(String qiniuAccessKey) {
        this.qiniuAccessKey = qiniuAccessKey;
    }

    public String getBucketName() {
        return bucketName;
    }

    public String getQiniuSecretKey() {
        return qiniuSecretKey;
    }

    public void setQiniuSecretKey(String qiniuSecretKey) {
        this.qiniuSecretKey = qiniuSecretKey;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public QiniuRegion getRegion() {
        return region;
    }

    public void setRegion(QiniuRegion region) {
        this.region = region;
    }

    public String getDomainOfBucket() {
        return domainOfBucket;
    }

    public void setDomainOfBucket(String domainOfBucket) {
        this.domainOfBucket = domainOfBucket;
    }

    /**
     * 区域名称： 华东   华北   华南   北美   东南亚
     *
     * @author novel
     * @date 2020/4/1
     */
    public enum QiniuRegion {
        /**
         * 华东
         */
        z0(Region.huadong()),
        /**
         * 华北
         */
        z1(Region.huabei()),
        /**
         * 华南
         */
        z2(Region.huanan()),
        /**
         * 北美
         */
        na0(Region.beimei()),
        /**
         * 东南亚
         */
        as0(Region.xinjiapo());

        private Region region;

        QiniuRegion(Region region) {
            this.region = region;
        }

        public Region getRegion() {
            return region;
        }
    }
}
