package com.novel.resource.minio.autoConfigurer;

import com.novel.resource.minio.config.MinioConfig;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * minio 配置类
 *
 * @author novel
 * @date 2020/1/3
 */
@Configuration
@ConditionalOnClass({MinioClient.class})
@EnableConfigurationProperties(MinioConfig.class)
@Slf4j
@AllArgsConstructor
public class MinioAutoConfiguration {
    private final MinioConfig minioConfig;

    @Scope("prototype")
    @Bean
    @ConditionalOnMissingBean(MinioClient.class)
    @ConditionalOnProperty(prefix = MinioConfig.MINIO_PREFIX, name = "url")
    public MinioClient minioClient() throws Exception {
        log.info("MinioClient 初始化...");
        MinioClient minioClient = MinioClient.builder().endpoint(minioConfig.getUrl(), minioConfig.getPort(), minioConfig.isSecure()).credentials(minioConfig.getAccessKey(), minioConfig.getSecretKey()).build();
        //检测资源桶是否存在，不存在子创建
        BucketExistsArgs bucketExistsArgs = BucketExistsArgs.builder().bucket(minioConfig.getBucketName()).build();
        if (!minioClient.bucketExists(bucketExistsArgs)) {
            MakeBucketArgs makeBucketArgs = MakeBucketArgs.builder().bucket(minioConfig.getBucketName()).build();
            minioClient.makeBucket(makeBucketArgs);
        }
        return minioClient;
    }
}
