package com.novel.service.impl;

import com.novel.domain.SysJobLog;
import com.novel.mapper.SysJobLogMapper;
import com.novel.service.SysJobLogService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 定时任务调度日志信息 服务层
 *
 * @author novel
 * @date 2020/3/2
 */
@Service
public class SysJobLogServiceImpl implements SysJobLogService {
    private final SysJobLogMapper jobLogMapper;

    public SysJobLogServiceImpl(SysJobLogMapper jobLogMapper) {
        this.jobLogMapper = jobLogMapper;
    }

    /**
     * 获取quartz调度器日志的计划任务
     *
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    @Override
    public List<SysJobLog> selectJobLogList(SysJobLog jobLog) {
        return jobLogMapper.selectJobLogList(jobLog);
    }

    /**
     * 通过调度任务日志ID查询调度信息
     *
     * @param id 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    @Override
    public SysJobLog selectJobLogById(Long id) {
        return jobLogMapper.selectJobLogById(id);
    }

    /**
     * 新增任务日志
     *
     * @param jobLog 调度日志信息
     * @return 结果
     */
    @Override
    public boolean addJobLog(SysJobLog jobLog) {
        return jobLogMapper.insertJobLog(jobLog) > 0;
    }

    /**
     * 批量删除调度日志信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public boolean deleteJobLogByIds(Long[] ids) {
        return jobLogMapper.deleteJobLogByIds(ids) > 0;
    }

    /**
     * 删除任务日志
     *
     * @param id 调度日志ID
     * @return 结果
     */
    @Override
    public boolean deleteJobLogById(Long id) {
        return jobLogMapper.deleteJobLogById(id) > 0;
    }

    /**
     * 清空任务日志
     *
     * @return 结果
     */
    @Override
    public boolean cleanJobLog() {
        jobLogMapper.cleanJobLog();
        return true;
    }
}
