package com.novel.controller;

import com.novel.common.utils.support.Convert;
import com.novel.domain.GenTable;
import com.novel.domain.GenTableColumn;
import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.service.IGenTableColumnService;
import com.novel.service.IGenTableService;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代码生成 操作处理
 *
 * @author novel
 * @date 2020/3/25
 */
@RestController
@RequestMapping("/tool/gen")
public class GenController extends BaseController {
    private final IGenTableService genTableService;
    private final IGenTableColumnService genTableColumnService;

    public GenController(IGenTableService genTableService, IGenTableColumnService genTableColumnService) {
        this.genTableService = genTableService;
        this.genTableColumnService = genTableColumnService;
    }

    /**
     * 获取数据表信息
     *
     * @param genTable 查询条件
     * @return 表信息
     */
    @RequiresPermissions("tool:gen:list")
    @GetMapping("/list")
    public TableDataInfo list(GenTable genTable) {
        startPage();
        List<GenTable> list = genTableService.selectGenTableList(genTable);
        return getDataTable(list);
    }

    /**
     * 修改代码生成业务
     *
     * @param tableId 表id
     * @return 结果
     */
    @RequiresPermissions("tool:gen:query")
    @GetMapping(value = "/{tableId}")
    public Result getInfo(@PathVariable Long tableId) {
        GenTable table = genTableService.selectGenTableById(tableId);
        List<GenTable> tables = genTableService.selectGenTableAll();
        List<GenTableColumn> list = genTableColumnService.selectGenTableColumnListByTableId(tableId);
        Map<String, Object> map = new HashMap<>(4);
        map.put("info" , table);
        map.put("rows" , list);
        map.put("tables" , tables);
        return toAjax(map);
    }

    /**
     * 查询数据库列表
     */
    @RequiresPermissions("tool:gen:list")
    @GetMapping("/db/list")
    public TableDataInfo dataList(GenTable genTable) {
        startPage();
        List<GenTable> list = genTableService.selectDbTableList(genTable);
        return getDataTable(list);
    }

    /**
     * 查询数据表字段列表
     */
    @RequiresPermissions("tool:gen:list")
    @GetMapping(value = "/column/{tableId}")
    public TableDataInfo columnList(@PathVariable Long tableId) {
        TableDataInfo dataInfo = new TableDataInfo();
        List<GenTableColumn> list = genTableColumnService.selectGenTableColumnListByTableId(tableId);
        dataInfo.setRows(list);
        dataInfo.setTotal(list.size());
        return dataInfo;
    }

    /**
     * 导入表结构（保存）
     *
     * @param tables 表名
     * @return 结果
     */
    @RequiresPermissions("tool:gen:list")
    @Log(title = "代码生成" , businessType = BusinessType.IMPORT)
    @PostMapping("/importTable")
    public Result importTableSave(String[] tables) {
//        String[] tableNames = Convert.toStrArray(tables);
        // 查询表信息
        List<GenTable> tableList = genTableService.selectDbTableListByNames(tables);
        genTableService.importGenTable(tableList);
        return success();
    }

    /**
     * 修改保存代码生成业务
     *
     * @param genTable 表信息
     * @return 结果
     */
    @RequiresPermissions("tool:gen:edit")
    @Log(title = "代码生成" , businessType = BusinessType.UPDATE)
    @PutMapping
    public Result editSave(@Validated @RequestBody GenTable genTable) {
        genTableService.validateEdit(genTable);
        genTableService.updateGenTable(genTable);
        return success();
    }

    /**
     * 删除代码生成
     *
     * @param tableIds 表id
     * @return 结果
     */
    @RequiresPermissions("tool:gen:remove")
    @Log(title = "代码生成" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{tableIds}")
    public Result remove(@PathVariable Long[] tableIds) {
        genTableService.deleteGenTableByIds(tableIds);
        return success();
    }

    /**
     * 预览代码
     *
     * @param tableId 表id
     * @return 结果
     */
    @RequiresPermissions("tool:gen:preview")
    @GetMapping("/preview/{tableId}")
    public Result preview(@PathVariable("tableId") Long tableId) {
        Map<String, String> dataMap = genTableService.previewCode(tableId);
        return toAjax(dataMap);
    }

    /**
     * 生成代码（下载方式）
     */
    @RequiresPermissions("tool:gen:code")
    @Log(title = "代码生成" , businessType = BusinessType.GENCODE)
    @GetMapping("/download/{tableName}")
    public void download(HttpServletResponse response, @PathVariable("tableName") String tableName) throws IOException {
        byte[] data = genTableService.downloadCode(tableName);
        genCode(response, data);
    }

    /**
     * 生成代码（自定义路径）
     *
     * @param tableName 表名
     * @return 结果
     */
    @RequiresPermissions("tool:gen:code")
    @Log(title = "代码生成" , businessType = BusinessType.GENCODE)
    @GetMapping("/genCode/{tableName}")
    public Result genCode(@PathVariable("tableName") String tableName) {
        genTableService.generatorCode(tableName);
        return success();
    }


    /**
     * 同步数据库
     *
     * @param tableName 表明
     * @return 结果
     */
    @RequiresPermissions("tool:gen:edit")
    @Log(title = "代码生成" , businessType = BusinessType.UPDATE)
    @GetMapping("/syncDb/{tableName}")
    public Result syncDb(@PathVariable("tableName") String tableName) {
        genTableService.syncDb(tableName);
        return success("数据同步成功！");
    }

    /**
     * 批量生成代码
     */
    @RequiresPermissions("tool:gen:code")
    @Log(title = "代码生成" , businessType = BusinessType.GENCODE)
    @GetMapping("/batchGenCode")
    public void batchGenCode(HttpServletResponse response, String tables) throws IOException {
        String[] tableNames = Convert.toStrArray(tables);
        byte[] data = genTableService.downloadCode(tableNames);
        genCode(response, data);
    }


    /**
     * 生成zip文件
     *
     * @param response 响应
     * @param data     数据
     * @throws IOException IO异常
     */
    private void genCode(HttpServletResponse response, byte[] data) throws IOException {
        response.reset();
        response.addHeader("Access-Control-Allow-Origin" , "*");
        response.addHeader("Access-Control-Expose-Headers" , "Content-Disposition");
        response.setHeader("Content-Disposition" , "attachment; filename=\"novel.zip\"");
        response.addHeader("Content-Length" , "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, response.getOutputStream());
    }

}
