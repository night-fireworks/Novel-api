package com.novel.constant;

import com.novel.common.constants.Constants;

/**
 * 常量
 *
 * @author novel
 * @date 2020/3/25
 */
public interface CommonConstant extends Constants {

    /**
     * 自动去除表前缀
     */
    public static String AUTO_REMOVE_PRE = "true";
}
