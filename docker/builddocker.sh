#!/bin/bash
#docker部分

echo '================打包完成，开始制作镜像================'

echo '================停止容器 novelapi================'
sudo docker stop novelapi
echo '================删除容器 novelapi================'
sudo docker rm novelapi
echo '================删除镜像 novelapi:latest================'
sudo docker rmi novelapi:latest
echo '================build 镜像 novelapi:latest================'
sudo docker build -t novelapi:latest  .
echo '================运行容器 novelapi================'
sudo docker run --name=novelapi --restart always -d -p 8880:8880 -v /resources:/resources -v /logs:/logs novelapi:latest

echo "finished!"
echo '================部署完成================'
