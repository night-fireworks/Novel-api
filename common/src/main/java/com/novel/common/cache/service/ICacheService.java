package com.novel.common.cache.service;


import com.novel.common.utils.model.Cache;

/**
 * 缓存信息服务
 *
 * @author novel
 * @date 2021/3/26 17:46
 */
public interface ICacheService {
    /**
     * 获取缓存信息
     *
     * @return 缓存信息
     */
    Cache getCacheInfo();
}
