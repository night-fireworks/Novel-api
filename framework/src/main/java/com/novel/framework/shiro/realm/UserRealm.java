package com.novel.framework.shiro.realm;

import cn.hutool.core.util.ObjectUtil;
import com.novel.common.constants.Constants;
import com.novel.common.utils.StringUtils;
import com.novel.framework.redis.ICacheService;
import com.novel.framework.shiro.jwt.JwtToken;
import com.novel.framework.shiro.jwt.utils.JWTUtil;
import com.novel.system.domain.LoginUser;
import com.novel.system.service.SysMenuService;
import com.novel.system.service.SysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

/**
 * 自定义Realm 处理登录 权限
 *
 * @author novel
 * @date 2020/3/2
 */
@Slf4j
public class UserRealm extends AuthorizingRealm {
    @Autowired
    private SysMenuService sysMenuService;
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private ICacheService iCacheService;

    /**
     * 大坑！，必须重写此方法，不然Shiro会报错<br/>
     * 设置realm支持的authenticationToken类型
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    /**
     * 只有当需要检测用户权限的时候才会调用此方法，例如checkRole,checkPermission之类的
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = JWTUtil.getSubject(principals.toString());
        String sessionId = JWTUtil.getId(principals.toString());
        LoginUser loginUser = (LoginUser) iCacheService.get(Constants.ACCOUNT + username + ":" + sessionId);
        // 角色列表
        Set<String> roles;
        // 功能列表
        Set<String> menus;
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        // 管理员拥有所有权限

        if (ObjectUtil.isNotNull(loginUser)) {
            if (loginUser.getUser().isAdmin()) {
                info.addRole("admin");
                info.addStringPermission("*:*:*");
            } else {
                menus = sysMenuService.selectPermsByUserId(loginUser.getUser().getId());
                roles = sysRoleService.selectRoleKeys(loginUser.getUser().getId());
                // 角色加入AuthorizationInfo认证对象
                info.setRoles(roles);
                // 权限加入AuthorizationInfo认证对象
                info.setStringPermissions(menus);
            }
        }
        return info;
    }

    /**
     * 登陆认证
     *
     * @param auth jwtFilter传入的token
     * @return 登陆信息
     * @throws AuthenticationException 未登陆抛出异常
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        String token = (String) auth.getCredentials();
        // 解密获得username，用于和数据库进行对比
        String username = JWTUtil.getSubject(token);
        String sessionId = JWTUtil.getId(token);
        if (StringUtils.isEmpty(username)) {
            throw new AuthenticationException("token 无效");
        }
        LoginUser loginUser = (LoginUser) iCacheService.get(Constants.ACCOUNT + username + ":" + sessionId);

        if (loginUser == null) {
            throw new AuthenticationException("用户未登录");
        }

        //校验token合法性和redis是否存在
        try {
            if (JWTUtil.verify(token, loginUser.getUser())) {
                return new SimpleAuthenticationInfo(token, token, getName());
            } else {
                throw new AuthenticationException("token 验证失败");
            }
        } catch (Exception e) {
            throw new AuthenticationException(e);
        }

    }

    /**
     * 清理缓存权限
     */
    public void clearCachedAuthorizationInfo() {
        this.clearCachedAuthorizationInfo(SecurityUtils.getSubject().getPrincipals());
    }

    @Override
    protected AuthorizationInfo getAuthorizationInfo(PrincipalCollection principals) {
        return super.getAuthorizationInfo(principals);
    }
}