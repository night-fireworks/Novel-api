package com.novel.system.service.impl;

import com.novel.common.constants.UserConstants;
import com.novel.common.exception.business.BusinessException;
import com.novel.common.utils.StringUtils;
import com.novel.common.utils.support.Convert;
import com.novel.system.domain.SysPost;
import com.novel.system.mapper.SysPostMapper;
import com.novel.system.mapper.SysUserPostMapper;
import com.novel.system.service.SysPostService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 岗位信息 服务层处理
 *
 * @author novel
 * @date 2019/6/11
 */
@Service
public class SysPostServiceImpl implements SysPostService {
    private final SysPostMapper postMapper;
    private final SysUserPostMapper userPostMapper;

    public SysPostServiceImpl(SysPostMapper postMapper, SysUserPostMapper userPostMapper) {
        this.postMapper = postMapper;
        this.userPostMapper = userPostMapper;
    }

    /**
     * 查询岗位信息集合
     *
     * @param post 岗位信息
     * @return 岗位信息集合
     */
    @Override
    public List<SysPost> selectPostList(SysPost post) {
        return postMapper.selectPostList(post);
    }

    /**
     * 查询所有岗位
     *
     * @return 岗位列表
     */
    @Override
    public List<SysPost> selectPostAll() {
        return postMapper.selectPostAll();
    }

    /**
     * 根据用户ID查询岗位
     *
     * @param userId 用户ID
     * @return 岗位列表
     */
    @Override
    public Map<String, Object> selectPostsByUserId(Long userId) {
        Map<String, Object> map = new HashMap<>();
        SysPost sysPost = new SysPost();
        sysPost.setStatus("0");
        List<SysPost> posts = postMapper.selectPostList(sysPost);
        map.put("posts", posts);
        if (StringUtils.isNotNull(userId)) {
            List<SysPost> userPosts = postMapper.selectPostsByUserId(userId);
            List<Long> postIds = new ArrayList<>();
            userPosts.forEach(item -> postIds.add(item.getId()));
            map.put("postIds", postIds);
        }
        return map;
    }

    /**
     * 根据用户id 查询岗位信息
     *
     * @param userId 用户id
     * @return 用户所有岗位信息
     */
    @Override
    public List<SysPost> selectPostListByUserId(Long userId) {
        return postMapper.selectPostsByUserId(userId);
    }

    /**
     * 通过岗位ID查询岗位信息
     *
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    @Override
    public SysPost selectPostById(Long postId) {
        return postMapper.selectPostById(postId);
    }

    /**
     * 批量删除岗位信息
     *
     * @param ids 需要删除的数据ID
     */
    @Override
    public boolean deletePostByIds(String ids) {
        Long[] postIds = Convert.toLongArray(ids);
        for (Long postId : postIds) {
            SysPost post = selectPostById(postId);
            if (countUserPostById(postId) > 0) {
                throw new BusinessException(String.format("岗位已分配,不能删除", post.getPostName()));
            }
        }
        return postMapper.deletePostByIds(postIds) > 0;
    }

    /**
     * 新增保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public boolean insertPost(SysPost post) {
        return postMapper.insertPost(post) > 0;
    }

    /**
     * 修改保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public boolean updatePost(SysPost post) {
        return postMapper.updatePost(post) > 0;
    }

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param postId 岗位ID
     * @return 结果
     */
    @Override
    public int countUserPostById(Long postId) {
        return userPostMapper.countUserPostById(postId);
    }

    /**
     * 校验岗位名称是否唯一
     *
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public String checkPostNameUnique(SysPost post) {
        long postId = StringUtils.isNull(post.getId()) ? -1L : post.getId();
        List<SysPost> postList = postMapper.checkPostNameUnique(post.getPostName());
        if (postList != null) {
            for (SysPost sysPost : postList) {
                if (StringUtils.isNotNull(sysPost) && sysPost.getId() != postId) {
                    return UserConstants.POST_NAME_NOT_UNIQUE;
                }
            }
        }

        return UserConstants.POST_NAME_UNIQUE;
    }

    /**
     * 校验岗位编码是否唯一
     *
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public String checkPostCodeUnique(SysPost post) {
        long postId = StringUtils.isNull(post.getId()) ? -1L : post.getId();
        List<SysPost> postList = postMapper.checkPostCodeUnique(post.getPostCode());
        if (postList != null) {
            for (SysPost sysPost : postList) {
                if (StringUtils.isNotNull(sysPost) && sysPost.getId() != postId) {
                    return UserConstants.POST_NAME_NOT_UNIQUE;
                }
            }
        }
        return UserConstants.POST_CODE_UNIQUE;
    }
}
