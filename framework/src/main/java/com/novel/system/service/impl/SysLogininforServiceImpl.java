package com.novel.system.service.impl;

import com.novel.system.domain.SysLogininfor;
import com.novel.system.mapper.SysLogininforMapper;
import com.novel.system.service.SysLogininforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统访问日志情况信息 服务层处理
 *
 * @author novel
 * @date 2019/12/20
 */
@Service
public class SysLogininforServiceImpl implements SysLogininforService {

    private final SysLogininforMapper logininforMapper;

    public SysLogininforServiceImpl(SysLogininforMapper logininforMapper) {
        this.logininforMapper = logininforMapper;
    }

    /**
     * 新增系统登录日志
     *
     * @param logininfor 访问日志对象
     */
    @Override
    public void insertLogininfor(SysLogininfor logininfor) {
        logininforMapper.insertLogininfor(logininfor);
    }

    /**
     * 查询系统登录日志集合
     *
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    @Override
    public List<SysLogininfor> selectLogininforList(SysLogininfor logininfor) {
        return logininforMapper.selectLogininforList(logininfor);
    }

    /**
     * 批量删除系统登录日志
     *
     * @param ids 需要删除的登录日志ID
     * @return
     */
    @Override
    public int deleteLogininforByIds(Long[] ids) {
        return logininforMapper.deleteLogininforByIds(ids);
    }

    /**
     * 清空系统登录日志
     */
    @Override
    public void cleanLogininfor() {
        logininforMapper.cleanLogininfor();
    }
}
