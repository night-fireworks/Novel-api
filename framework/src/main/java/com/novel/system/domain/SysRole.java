package com.novel.system.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.novel.framework.base.BaseModel;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * 角色表 sys_role
 *
 * @author novel
 * @date 2019/4/16
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class SysRole extends BaseModel {
    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
    @NotBlank(message = "角色名称不能为空", groups = {AddGroup.class})
    @Size(max = 10, message = "角色名称长度不能超过10个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "角色名称")
    private String roleName;

    /**
     * 角色权限
     */
    @NotBlank(message = "权限字符不能为空", groups = {AddGroup.class})
    @Size(max = 10, message = "权限字符长度不能超过10个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "权限字符")
    private String roleKey;

    /**
     * 角色排序
     */
    @NotBlank(message = "角色排序不能为空", groups = {AddGroup.class})
    @Range(min = 0, message = "角色排序不正确", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "角色排序")
    private String roleSort;

    /**
     * 角色状态（0正常 1停用）
     */
    @NotBlank(message = "角色状态不能为空", groups = {AddGroup.class})
    @Pattern(regexp = "^0|1$", message = "角色状态错误", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "角色状态", replace = {"正常_0", "停用_1"})
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 菜单组
     */
    private Long[] menuIds;
}